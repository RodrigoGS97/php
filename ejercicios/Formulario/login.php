<?php
    session_start();
    if(empty($_SESSION['Alumno'])){
        $_SESSION['Alumno'] =[
            1 =>[
                'num_cta' => '1',
                'nombre'  => 'Admin',
                'primer_apellido' => 'General',
                'segundo_apellido' => '',
                'contrasena' => 'adminpass123.',
                'genero' => 'O',
                'fecha_nac' => '1994-02-28'
            ]
        ]; 
    }

?>


<html>
    <head>
        <title>login</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="css/style.css?ts=<?=time()?>">
    </head>
    <body>
        <div class="container">
            <div id="ventanaLogin">
                <h1>Login</h1>
                <form action="procesar_login.php?accion=get&texto=textoenget" method="POST">
                    <label class="form-label" for="input-text">Numero de cuenta:</label><br>
                    <input name="texto" class="form-input " type="text" id="input-numCuenta" placeholder="Numero de cuenta">
                    <br>
                    <label class="form-label" for="input-text">Contraseña</label><br>
                    <input name="contrasena" class="form-input " type="password" id="input-contrasena" placeholder="Contraseña">
                    <br>
                    <input type='submit' class="btn" value="Entrar"/>
                </form>

            </div>

        </div>
    </body>
</html>

