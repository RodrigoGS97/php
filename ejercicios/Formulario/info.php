<?php
    session_start();
    if(empty($_COOKIE['Alumnoc'])){
        header('Location: login.php');
    }
    else{
        $user=$_COOKIE['Alumnoc'];
    }

?>

<html>
    <head>
        <title>info</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/style.css?ts=<?=time()?>">
    </head>
    <body>
        <div class="container_info">
            <header>
            <a class="cabezera" href='./info.php'>Home</a>
            <a class="cabezera" href='./formulario.php'>Registrar Alumnos</a>
            <a class="cabezera" href='./cerrarSesion.php'>Cerrar Sesión</a>

            </header>
            <h1>Usuario Autenticado</h1>
            <div id="ventanaUsuario">
                <h1><?php echo$_SESSION['Alumno'][$user]['nombre'].' ';
                          echo$_SESSION['Alumno'][$user]['primer_apellido'];
                ?></h1>
                <h2>Información</h2>
                <?php echo"<h2>Número de cuenta: ".$_SESSION['Alumno'][$user]['num_cta']."</h2>";
                        echo"<h2>Fecha de nacimiento: ".$_SESSION['Alumno'][$user]['fecha_nac']."</h2>";
                ?>
            </div>
            <h1>Datos guardados:</h1>
            <table id="tabladatos">
                <tbody>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Fecha de Nacimiento</th>
                    </tr>
                    <?php
                        foreach($_SESSION['Alumno'] as $llave => $valor){
                            echo"<tr>";
                            echo"<th>".$llave."</th>";
                            echo"<td>".$valor['nombre'].' '.$valor['primer_apellido'].' '.$valor['segundo_apellido']."</td>";
                            echo"<td>".$valor['fecha_nac']."</td>";
                            echo"<tr>";
                        }
                    ?>
                </tbody>
            </div>

        </div>
    </body>
</html>

