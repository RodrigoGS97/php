<?php
    session_start();
    if(empty($_COOKIE['Alumnoc'])){
        header('Location: login.php');
    }
    else{
        $user=$_SESSION['Alumno'][$_COOKIE['Alumnoc']];
    }

?>

<html>
    <head>
        <title>info</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/style.css?ts=<?=time()?>">
    </head>
    <body>
        <div class="container_info">
            <header>
                <a class="cabezera" href='./info.php'>Home</a>
                <a class="cabezera" href='./formulario.php'>Registrar Alumnos</a>
                <a class="cabezera" href='./cerrarSesion.php'>Cerrar Sesión</a>
            </header>
            <form action="procesar_form.php?accion=get&texto=textoenget" method="POST">
                <label class="form-label" for="input-text">Número de cuenta</label>
                <input name="numeroCuenta" class="form-input " type="text" id="input-numCuenta" placeholder="Número de cuenta">
                <br>
                <label class="form-label" for="input-text">Nombre</label>
                <input name="nombre" class="form-input " type="text" id="input-nombre" placeholder="Nombre">
                <br>
                <label class="form-label" for="input-text">Primer apellido</label>
                <input name="primerApellido" class="form-input " type="text" id="input-apellido" placeholder="Primer apellido">
                <br>
                <label class="form-label" for="input-text">Segundo apellido</label>
                <input name="segundoApellido" class="form-input " type="text" id="input-apellido" placeholder="Segundo apellido">
                <br>
                <label class="form-label" for="input-text">Género</label>
                <div id="opciones">
                    Hombre<input name="genero" class="form-input " type="radio" value="H">
                    Mujer<input name="genero" class="form-input " type="radio" value="M">
                    Otro<input name="genero" class="form-input " type="radio" value="O">
                </div>
                <br>
                <label class="form-label" for="input-text">Fecha de nacimiento</label>
                <input name="fecha_nacimiento" class="form-input " type="date" id="input-fecnac" placeholder="dd/mm/aaaa">
                <br>
                <label class="form-label" for="input-text">Contraseña</label>
                <input name="contrasena" class="form-input " type="password" id="input-contra" placeholder="Contraseña">
                <br>
                <input type='submit' class="btn" value="Registrar"/>
                
            </form>
        </div>
    </body>





</html>